import        React               from 'react';
import type { NextPage          } from 'next';
import      { Flex              } from '@chakra-ui/react';
import      { Input             } from '@chakra-ui/react';
import      { Button            } from '@chakra-ui/react';
import      { Heading           } from '@chakra-ui/react';
import      { useColorMode      } from '@chakra-ui/react';
import      { useColorModeValue } from '@chakra-ui/react';

const Home: NextPage = () => {

  const { toggleColorMode } = useColorMode();

  const formBackground = useColorModeValue('gray.100', 'gray.700');

  return (
    <Flex height="100vh" alignItems="center" justifyContent="center" >
      <Flex direction="column" background={formBackground} p={12} rounded={6} >
        <Heading mb={6} >Log in</Heading>

        <Input placeholder="your email" variant="filled" mb={3} type="email" />
        <Input placeholder="your password" variant="filled" mb={6} type="password" />

        <Button mb={6} colorScheme="teal" >Log in</Button>
        <Button onClick={toggleColorMode} >Toggle Color Mode</Button>
      </Flex>
    </Flex>
  );
};

export default Home;
