# structure-frontend-react-with-next
* Next.js v13 개발을 위한 환경 설명과 기초 개발 흐름을 안내한다.


# 이력
* v0.0.1
    * 2023.04.17
        * 최초 등록
* v0.0.2
    * 2023.04.19
        * 패키지 구조 설명 추가
        * 예제 추가

# 구성
* 환경
    * windows 10, mac
    * WebStorm 2023.1
    * node v16.8.0
    * npm v7.21.0

<!-- blank line -->
* 라이브러리
  * react v18.2.0 (<a href="https://ko.reactjs.org/">Link</a>)
  * next v13.2.4 (<a href="https://nextjs.org/">Link</a>)
  * typescript v4.9.5 (<a href="https://www.typescriptlang.org/">Link</a>)
  * axios v1.3.5 (<a href="https://axios-http.com/kr/">Link</a>)
  * eslint v8.22.0 (<a href="https://eslint.org/">Link</a>)
  * chakra-ui v2.5.4 (<a href="https://chakra-ui.com/">Link</a>)

# 설치 및 실행
<!-- blank line -->
* 설치
    * Node 모듈 설치 (<a href="https://nodejs.org/ko/">Link</a>)
    * Git 설치 (<a href="https://git-scm.com/">Link</a>)
    * Webstrom 설치 (<a href="https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows">Link</a>)

* 실행
    * npm i
    * npm run dev

# 개발
<!-- blank line -->
## 패키지 및 파일 명명 규칙
* 모든 화면 파일명(*.tsx)은 "kebab-case"로 작성한다.
    * Good: sample-list.tsx
    * Bad: SampleList.tsx
* 모든 화면 파일명(*.tsx)은 기능의 해당하는 두가지 단어 이상으로 작성한다.
    * Good: sample-list.tsx
    * Bad: sample.tsx
* 모든 기능 파일명(*.ts)은 "kebab-case" + "dot.case"로 작성한다.
    * Good: platform.service.ts
    * Bad: platform-service.ts
    * 기능 ex) service, model, router, service...
* 모든 폴더명은 단수로 작성한다.
    * Good: component, service, enum
    * Bad: components, services, enums
* 모든 폴더명은 "kebab-case"로 작성한다.
    * Good: platform, tv-platform
    * Bad: TvPlatform, tvPlatform
* 기타 파일명은 "kebab-case"로 작성한다.
    * Good: style.css, default-style.css
    * Bad: defaultStyle.css

## 패키지 구조
```
├── public
│   └── assets
│       ├── css
│       ├── font
│       └── images
│           ├── common
│           └── icon
├── src
│   ├── app
│   │   ├── common
│   │   │   ├── api
│   │   │   ├── component
│   │   │   ├── constant
│   │   │   ├── enum
│   │   │   ├── hook
│   │   │   ├── model
│   │   │   ├── service
│   │   │   └── store
│   │   ├── domain
│   │   │   └── {{비즈니스}}
│   │   │       ├── component
│   │   │       │   └── wrapper
│   │   │       ├── constant
│   │   │       ├── enum
│   │   │       ├── hook
│   │   │       ├── model
│   │   │       ├── service
│   │   │       └── store
│   │   └── view
│   │       └── {{비즈니스}}
│   │           ├── add-view.tsx
│   │           ├── detail-view.tsx
│   │           ├── list-view.tsx
│   │           └── modify-view.tsx
│   ├── core
│   ├── pages
│   │   ├── {{비즈니스}}
│   │   │   ├── detail
│   │   │   │   └── [id].tsx 
│   │   │   ├── modify
│   │   │   │   └── [id].tsx 
│   │   │   ├── add.tsx    
│   │   │   └── index.tsx 
│   │   ├── _app.tsx
│   │   └── index.tsx   
│   ├── hooks.ts
│   ├── middleware.ts
│   └── store.ts
├── env.development
├── env.local
├── env.product
├── .eslintignore
├── .eslintrc
├── .gitignore
├── next.config.js
├── next-env.d.ts
├── package.json
├── README.md
└── tsconfig.json
```
## 상위 패키지 구조 설명
| Package         | Description              |
  |-----------------|--------------------------|
| /node_modules   | npm 라이브러리 영역             |
| /public         | 정적 파일 영역                 |
| /src/app        | 주요구성요소 구현 영역             |
| /src/app/common | 공통으로 사용되는 주요구성요소 구현 영역   |
| /src/app/domain | 비즈니스별로 사용되는 주요구성요소 구현 영역 |
| /src/app/view   | 서비스 기반 화면 구현 영역          |
| /src/core       | 프로젝트 코어 구성요소 영역          |
| /src/pages      | 라우터 기반 화면 구현 영역          |

## 패키지 구조 설명
| Package                   | Description          |
  |---------------------------|----------------------|
| /public/asset/css         | 전체 css 파일 영역         |
| /public/asset/font        | 전체 폰트 파일 영역          |
| /public/asset/image       | 전체 이미지 파일 영역         |
| /src/app/common/api       | 화면 공통 api 구성 영역      |
| /src/app/common/component | 화면 공통 component 구성 영역 |
| /src/app/common/constant  | 화면 공통 상수 구성 영역       |
| /src/app/common/enum      | 화면 공통 enum 구성 영역     |
| /src/app/common/hook      | 화면 공통 hook 구성 영역     |
| /src/app/common/model     | 화면 공통 model 구성 영역    |
| /src/app/common/service   | 화면 공통 service 구성 영역  |
| /src/app/common/store     | 화면 공통 상태 관리 구성 영역    |
| /src/core/decorator       | 전역 decorator 구성 영역   |
| /src/core/service         | 전역 service 구성 영역     |

## 도메인 별 패키지 구조
  ```
  ├── api
  ├── component
  │   └── wrapper
  ├── constant
  ├── enum
  ├── hook
  ├── moel
  ├── service
  └── store
  ```

## 도메인 별 패키지 구조 설명
| Package            | Description     |
  |--------------------|-----------------|
| /api               | api 영역          |
| /component         | VAC 컴포넌트 영역     |
| /component/wrapper | Wrapper 컴포넌트 영역 |
| /constant          | 상수 영역           |
| /model             | 모델 영역           |
| /enum              | Enum 영역         |
| /service           | 서비스 영역          |
| /store             | 상태 관리 영역        |

# 코딩 명명 규칙
## 함수명
| 함수명          | 설명 |
  |  --------      |  -------- |
| onAdd          | 등록 페이지 이동 |
| onModify       | 수정 페이지 이동 |
| onCancel       | 이전 페이지 이동 |
| onRow          | 목록 테이블 ROW 데이터 클릭 |
| onSave         | 등록 FORM Action |
| onUpdate       | 수정 FORM Action |
| onDelete       | 삭제 FORM Action |
| onSearch       | 검색 FORM Action |

## Action
| Action 명       | 설명 |
  |  --------       |  -------- |
| getAll          | 목록 조회 |
| getOne          | 상세 조회 |
| getPage         | 목록 페이지 조회 |
| add             | 등록 |
| update          | 수정 |
| delete          | 삭제 |

## 접근제어자
* 모든 public 접근제어자는 생략한다.
* 화면에서 사용하는 변수, 함수는 public 이다.
* class 내부적으로 사용하는 함수, 변수는 private를 명시적으로 선언한다.

## 명명규칙
| 단위       | 설명                          |
  |----------|-----------------------------|
| variable | camelCase                   |
| function | camelCase                   |
| class    | PascalCase                  |
| folder   | kebab-case                  |
| *.ts     | kebab-case + dot.case + .ts |
| *.css    | kebab-case + .*             |
| *.tsx    | kebab-case + .tsx           |
| *.other  | kebab-case + .*             |

# 예제
## enum
* 작성언어: typescript
* enum 변수는 `SCREAMING_SNAKE_CASE`로 작성한다.
```typescript
export namespace Enum {
  export namespace SAMPLE {
    export namespace ALBUM {
      export enum TEST {
        TEST_1 = 'TEST_1',
        TEST_2 = 'TEST_2'
      }
    }
  }

  export namespace CORE {
    export enum ENV {
      LOCAL = 'local',
      DEV   = 'development',
      PROD  = 'production'
    }

    export namespace STORAGE {
      export enum KEY {
        TEST = 'TEST'
      }

      export enum TYPE {
        STRING  = 'string',
        NUMBER  = 'number',
        BOOLEAN = 'boolean',
        OBJECT  = 'object',
        ARRAY   = 'array',
      }
    }

    export enum SPINNER {
      DEFAULT       = 'DEFAULT',
      WRITE         = 'WRITE',
      SPINNER_ERROR = 'SPINNER_ERROR'
    }

    export enum DATE_FORMAT {
      RETURN_DATE_TIME        = 'YYYY-MM-DDTHH:mm',
      RETURN_DATE_TIME_SECOND = 'YYYY-MM-DDTHH:mm:ss',

      DISPLAY_DATE_TIME        = 'YYYY-MM-DD HH:mm',
      DISPLAY_DATE_TIME_SECOND = 'YYYY-MM-DD HH:mm:ss'
    }
  }
}
```

## model
* 작성언어: typescript
* Request / Response - API 요청, 수신 기준으로 Request / Response로 분할하여 작성
* Request Validator는 `class-validator` 를 사용하여 작성
* Object, Array의 하위항목은 `@ValidateNested`를 사용하여 작성
* 필수항목은 `@IsNotEmpty`, 선택항목은 `@IsOptional`을 사용함
```typescript
import                      'reflect-metadata';
import { Expose      } from 'class-transformer';
import { IsString    } from 'class-validator';
import { IsNumber    } from 'class-validator';
import { IsNotEmpty  } from 'class-validator';

import { Description } from '@/src/core/decorator/description.decorator';

export namespace Album {
  export namespace Request {
    export class Add {
      @Attribute('사용자-아이디')
      @IsNumber() @IsNotEmpty()
      userId!: number;

      @Attribute('제목')
      @IsString() @IsNotEmpty()
      title!: string;
    }
  }

  export namespace Response {
    export class FindAll {
      @Attribute('사용자-아이디')
      userId!: number;

      @Attribute('아이디')
      id!: number;

      @Attribute('제목')
      title!: string;
    }

    export class FindOne {
      @Attribute('사용자-아이디')
      userId!: number;

      @Attribute('아이디')
      id!: number;

      @Attribute('제목')
      title!: string;
    }
  }
}
```
